package com.example.skill.demo.repository;

import com.example.skill.demo.model.MeetingRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MeetingRoomRepository extends JpaRepository<MeetingRoom,Integer> {

}
