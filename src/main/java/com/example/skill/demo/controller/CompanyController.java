package com.example.skill.demo.controller;

import com.example.skill.demo.model.Company;
import com.example.skill.demo.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/companies")
public class CompanyController {
    @Autowired
    CompanyRepository companyRepository;
    @GetMapping("")
    public ResponseEntity showSkills() {
        List<Company> skills = companyRepository.findAll();
        return ResponseEntity.ok(skills);

    }

    @PostMapping("")
    public ResponseEntity createCompany(@RequestBody Company company) {
        Company newCompany = new Company();
        newCompany.setName(company.getName());
        return ResponseEntity.ok(companyRepository.save(newCompany));
    }

    @PutMapping("/{id}")
    public ResponseEntity editCompany(@PathVariable("id") Integer id ,@RequestBody Company company) {
        Optional<Company> optionalCompany = companyRepository.findById(id);
        if(optionalCompany == null) {
            return ResponseEntity.badRequest().body("Peerson under id" +id +" not found");
        }
        Company exsistingSkill = optionalCompany.get();
        exsistingSkill.setName(company.getName());
        return ResponseEntity.ok(companyRepository.save(exsistingSkill));
    }

    @GetMapping("/{id}")
    public ResponseEntity getCompany(@PathVariable("id") Integer id) {
        Optional<Company> company = companyRepository.findById(id);
        if(company == null) {
            return ResponseEntity.badRequest().body("company not found");
        }
        return ResponseEntity.ok(company.get());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteCompany(@PathVariable("id") Integer id) {
        Optional<Company> company = companyRepository.findById(id);
        if(company == null) {
            return ResponseEntity.badRequest().body("company not found");
        }
        companyRepository.deleteById(id);
        return ResponseEntity.ok().body("Deleted Succesfully");
    }


}

