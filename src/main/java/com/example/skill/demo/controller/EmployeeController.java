package com.example.skill.demo.controller;

import com.example.skill.demo.model.Employee;
import com.example.skill.demo.model.MeetingRoom;
import com.example.skill.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<Employee> getSingleEmployee(@PathVariable Integer id) {
        return employeeRepository.findById(id);
    }

    @PostMapping("")
    ResponseEntity addNewEmployee(@Validated @RequestBody Employee employee) {
        return ResponseEntity.ok(employeeRepository.save(employee));
    }

    @PutMapping("/{id}")
    ResponseEntity updateEmployee(@PathVariable Integer id, @RequestBody Employee employee) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);
        if(optionalEmployee.isPresent()) {
            Employee existingEmployee = optionalEmployee.get();
            existingEmployee.setCompany(employee.getCompany());
            existingEmployee.setName(employee.getName());
            return ResponseEntity.ok(employeeRepository.save(existingEmployee));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteEmployee(@PathVariable Integer id) {
        Optional<Employee> employee = employeeRepository.findById(id);
        if(employee.isPresent()) {
            employeeRepository.deleteById(id);
            return ResponseEntity.ok().body("Deleted Successfully");
        }
        return ResponseEntity.notFound().build();
    }


}
