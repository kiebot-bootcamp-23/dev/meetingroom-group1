package com.example.skill.demo.controller;

import com.example.skill.demo.model.Slot;
import com.example.skill.demo.repository.SlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/slots")
public class SlotController {

    @Autowired
    SlotRepository slotRepository;

    @GetMapping("")
    List<Slot> showPerson() {
        return slotRepository.findAll();
    }


    @GetMapping("/{id}")
    ResponseEntity slotById(@PathVariable("id") Integer id) {
        final Optional<Slot> optionalPerson = slotRepository.findById(id);

        if (optionalPerson.isPresent()) {
            return ResponseEntity.ok(optionalPerson.get());
        }
        return ResponseEntity.badRequest().body("Could not find the slot in this id " + id);
    }


    @PostMapping("")
    ResponseEntity createSlot(@RequestBody Slot slot, UriComponentsBuilder uriComponentsBuilder) {
        final Slot createSlot = slotRepository.save(slot);
        return ResponseEntity.created(uriComponentsBuilder.path("/api/slots").buildAndExpand(createSlot.getId()).toUri()).body(createSlot);
    }

    @PutMapping("{id}")
    ResponseEntity updateSlotById(@PathVariable("id") Integer id, @RequestBody Slot slot) {
        final Optional<Slot> optionalSlot = slotRepository.findById(id);
        if (optionalSlot.isEmpty()) {
            return ResponseEntity.badRequest().body("Could not find the slot in this id " + id);
        }
            Slot existingSlot = optionalSlot.get();
            existingSlot.setStartTime(slot.getStartTime());
            existingSlot.setEndTime(slot.getEndTime());
            return ResponseEntity.ok(slotRepository.save(existingSlot));
        }


    @DeleteMapping("{id}")
    ResponseEntity deleteSlot(@PathVariable("id") Integer id){
        final Optional<Slot> optionalSlot = slotRepository.findById(id);
        if(optionalSlot.isPresent()) {
            slotRepository.deleteById(id);
            return ResponseEntity.ok().body("slot deleted");
        }
        return ResponseEntity.badRequest().body("Could not find the slot in this id " + id);
    }
}
