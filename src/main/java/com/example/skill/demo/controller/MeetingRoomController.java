package com.example.skill.demo.controller;

import com.example.skill.demo.model.MeetingRoom;
import com.example.skill.demo.repository.MeetingRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/meeting-rooms")
public class MeetingRoomController {

    @Autowired
    MeetingRoomRepository meetingRoomRepository;

    @GetMapping("")
    public List<MeetingRoom> getAllMeetingRooms() {
        return meetingRoomRepository.findAll();
    }

    @GetMapping("/{id}")
    public Optional<MeetingRoom> getSingleMeetingRoom(@PathVariable Integer id) {
        return meetingRoomRepository.findById(id);
    }

    @PostMapping("")
    ResponseEntity addNewMeetingRoom(@Validated @RequestBody MeetingRoom meetingRoom) {
        return ResponseEntity.ok(meetingRoomRepository.save(meetingRoom));
    }

    @PutMapping("/{id}")
    ResponseEntity updateMeetingRoom(@PathVariable Integer id, @RequestBody MeetingRoom meetingRoom) {
        Optional<MeetingRoom> optionalMeetingRoom = meetingRoomRepository.findById(id);
        if(optionalMeetingRoom.isPresent()) {
            MeetingRoom existingMeetingRoom = optionalMeetingRoom.get();
            existingMeetingRoom.setMeetingRoomName(meetingRoom.getMeetingRoomName());
            existingMeetingRoom.setCapacity(meetingRoom.getCapacity());
            existingMeetingRoom.setIsBooked(meetingRoom.getIsBooked());
            existingMeetingRoom.setPricePerSlot(meetingRoom.getPricePerSlot());
            return ResponseEntity.ok(meetingRoomRepository.save(existingMeetingRoom));
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{id}")
    ResponseEntity deleteMeetingRoom(@PathVariable Integer id) {
        Optional<MeetingRoom> meetingRoom = meetingRoomRepository.findById(id);
        if(meetingRoom.isPresent()) {
            meetingRoomRepository.deleteById(id);
            return ResponseEntity.ok().body("Deleted Successfully");
        }
        return ResponseEntity.notFound().build();
    }

}
