package com.example.skill.demo.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter


public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    private String name;
//    @OneToMany(mappedBy = "company")
//    List<Employee> employees;

    public Company(Integer id) {
        Id = id;
    }
}

