package com.example.skill.demo.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MeetingRoom {
    @Id
    @GeneratedValue
    Integer id;

    String meetingRoomName;

    Boolean isBooked;

    Integer pricePerSlot;

    Integer capacity;

}
