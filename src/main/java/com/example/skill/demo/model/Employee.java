package com.example.skill.demo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Employee {
    @Id
    @GeneratedValue
    Integer id;

    String name;

    @ManyToOne
    @JoinColumn(name="company_id")
    @JsonIgnore
    private Company company;
}
